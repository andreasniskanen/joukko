joukko : SAT-based Solver for Argumentation Frameworks with Collective Attacks
==============================================================================

Version    : 2020.03.24

Author     : [Andreas Niskanen](mailto:andreas.niskanen@helsinki.fi), University of Helsinki


Compiling
---------
Please choose desired SAT solver (glucose, glucose-inc) by uncommenting the corresponding line in the Makefile.

To compile ``joukko``:
```
make
```

To remove all object files:
```
make clean
```


Usage
-----
```
./joukko -p <task> -f <file> [-a <query>]

  <task>      computational problem
  <file>      input SETAF in APX format
  <query>     query argument
Options:
  --help      Displays this help message.
  --version   Prints version and author information.
  --formats   Prints available file formats.
  --problems  Prints available computational tasks.
```


Input format
------------
For input SETAFs, ``joukko`` uses the APX syntax described on the [ASPARTIX webpage](https://www.dbai.tuwien.ac.at/research/argumentation/aspartix/setaf.html):
```
            arg(a).    ... a is an argument
            att(x,b).  ... the attack named x attacks argument b
            mem(x,a).  ... the argument a is in the support of attack x
```
See ``example.apx`` for an example SETAF.


Dependencies
------------
SAT solver [Glucose](https://www.labri.fr/perso/lsimon/glucose/) (version 4.1) is included in this release.


Contact
-------
Please direct any questions, comments, bug reports etc. directly to [the author](mailto:andreas.niskanen@helsinki.fi).
