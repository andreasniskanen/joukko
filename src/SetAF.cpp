/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <iostream>

#include "SetAF.h"

using namespace std;

SetAF::SetAF() : args(0), atts(0), count(0) {}

void SetAF::add_argument(string arg)
{
	if (arg_to_int.count(arg) == 0) {
		int_to_arg.push_back(arg);
		arg_to_int[arg] = args++;
	} else {
		cerr << "Fatal error: Two arguments have the same identifier " << arg << ".\n";
		exit(1);
	}
}

void SetAF::add_attack(pair<string,string> att)
{
	if (att_to_int.count(att.first) == 0) {
		int_to_att.push_back(att.first);
		att_to_int[att.first] = atts++;
	}
	uint32_t attack = att_to_int[att.first];
	if (arg_to_int.count(att.second) != 0) {
		uint32_t target = arg_to_int[att.second];
		attackers[target].push_back(attack);
	} else {
		cerr << "Fatal error: att(" << att.first << "," << att.second << ") contains an invalid indentifier.\n";
		exit(1);
	}
}

void SetAF::add_argument_to_attack(pair<string,string> mem)
{
	if (att_to_int.count(mem.first) != 0 && arg_to_int.count(mem.second) != 0) {
		uint32_t att = att_to_int[mem.first];
		uint32_t arg = arg_to_int[mem.second];
		attacking_sets[att].push_back(arg);
	} else {
		cerr << "Fatal error: mem(" << mem.first << "," << mem.second << ") contains an invalid indentifier.\n";
		exit(1);
	}
}

void SetAF::init_attackers()
{
	attackers.resize(args);
}

void SetAF::init_attacking_sets()
{
	attacking_sets.resize(atts);
}

void SetAF::init_vars()
{
	arg_var.resize(args);
	set_is_active_var.resize(atts);
	attacked_by_active_set_var.resize(atts);
	
	for (uint32_t i = 0; i < args; i++) {
		arg_var[i] = ++count;
	}

	if (sem == STG || sem == SST) {
		range_var.resize(args);
		for (uint32_t i = 0; i < args; i++) {
			range_var[i] = ++count;
		}
	}

	for (uint32_t i = 0; i < atts; i++) {
		set_is_active_var[i] = ++count;
	}

	for (uint32_t i = 0; i < atts; i++) {
		attacked_by_active_set_var[i] = ++count;
	}
}