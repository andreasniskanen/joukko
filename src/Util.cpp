/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <iostream>

#include "Encodings.h"
#include "Util.h"

using namespace std;

void print_begin(bool new_line)
{
	string bracket = "[";
	if (new_line) bracket += "\n";
	cout << bracket << flush;
}

void print_end(bool new_line)
{
	string bracket = "]";
	if (new_line) bracket += "\n";
	cout << bracket << flush;
}

void print_yes(bool comma)
{
	string answer = "YES";
	if (comma) answer += ", ";
	cout << answer << flush;
}

void print_no(bool comma)
{
	string answer = "NO";
	if (comma) answer += ", ";
	cout << answer << flush;
}

void print_single_extension(const SetAF & af, const std::vector<uint32_t> & extension, bool tab)
{
	if (tab) cout << "\t";
	cout << "[";
	for (uint32_t i = 0; i < extension.size(); i++) {
		cout << af.int_to_arg[extension[i]];
		if (i != extension.size()-1) cout << ",";
	}
	cout << "]" << endl;
}

void print_extensions(const SetAF & af, const std::vector<std::vector<uint32_t>> & extensions)
{
	cout << "\t[\n";
	for (uint32_t i = 0; i < extensions.size(); i++) {
		cout << "\t\t[";
		for (uint32_t j = 0; j < extensions[i].size(); j++) {
			cout << af.int_to_arg[extensions[i][j]];
			if (j != extensions[i].size()-1) cout << ",";
		}
		cout << "]" << "\n";
	}
	cout << "\t]" << endl;
}