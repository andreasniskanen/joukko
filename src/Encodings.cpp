/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"

using namespace std;

namespace Encodings {

void add_set_is_active_clauses(const SetAF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.atts; i++) {
		for (uint32_t j = 0; j < af.attacking_sets[i].size(); j++) {
			vector<int> clause = { -af.set_is_active_var[i], af.arg_var[af.attacking_sets[i][j]] };
			solver.add_clause(clause);
		}
		vector<int> clause(af.attacking_sets[i].size()+1);
		for (uint32_t j = 0; j < af.attacking_sets[i].size(); j++) {
			clause[j] = -af.arg_var[af.attacking_sets[i][j]];
		}
		clause[af.attacking_sets[i].size()] = af.set_is_active_var[i];
		solver.add_clause(clause);
	}
}

void add_attacked_by_active_set_clauses(const SetAF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.atts; i++) {
		for (uint32_t j = 0; j < af.attacking_sets[i].size(); j++) {
			for (uint32_t k = 0; k < af.attackers[af.attacking_sets[i][j]].size(); k++) {
				vector<int> clause = { af.attacked_by_active_set_var[i], -af.set_is_active_var[af.attackers[af.attacking_sets[i][j]][k]] };
				solver.add_clause(clause);
			}
		}
		vector<int> clause;
		for (uint32_t j = 0; j < af.attacking_sets[i].size(); j++) {
			for (uint32_t k = 0; k < af.attackers[af.attacking_sets[i][j]].size(); k++) {
				clause.push_back(af.set_is_active_var[af.attackers[af.attacking_sets[i][j]][k]]);
			}
		}
		clause.push_back(-af.attacked_by_active_set_var[i]);
		solver.add_clause(clause);
	}
}

void add_range(const SetAF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.args; i++) {
		vector<int> short_clause = { af.range_var[i], -af.arg_var[i] };
		solver.add_clause(short_clause);
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			vector<int> clause = { af.range_var[i], -af.set_is_active_var[af.attackers[i][j]] };
			solver.add_clause(clause);
		}
		vector<int> long_clause(af.attackers[i].size()+2);
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			long_clause[j] = af.set_is_active_var[af.attackers[i][j]];
		}
		long_clause[af.attackers[i].size()] = af.arg_var[i];
		long_clause[af.attackers[i].size()+1] = -af.range_var[i];
		solver.add_clause(long_clause);
	}
}

void add_conflict_free(const SetAF & af, SAT_Solver & solver)
{
	add_set_is_active_clauses(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			vector<int> clause = { -af.arg_var[i], -af.set_is_active_var[af.attackers[i][j]] };
			solver.add_clause(clause);
		}
	}
}

void add_admissible(const SetAF & af, SAT_Solver & solver)
{
	add_conflict_free(af, solver);
	add_attacked_by_active_set_clauses(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			vector<int> clause = { -af.arg_var[i], af.attacked_by_active_set_var[af.attackers[i][j]] };
			solver.add_clause(clause);
		}
	}
}

void add_complete(const SetAF & af, SAT_Solver & solver)
{
	add_admissible(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		vector<int> clause(af.attackers[i].size()+1);
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			clause[j] = -af.attacked_by_active_set_var[af.attackers[i][j]];
		}
		clause[af.attackers[i].size()] = af.arg_var[i];
		solver.add_clause(clause);
	}
}

void add_stable(const SetAF & af, SAT_Solver & solver)
{
#if defined(AD_IN_ST)
	add_admissible(af, solver);
#elif defined(CO_IN_ST)
	add_complete(af, solver);
#else
	add_conflict_free(af, solver);
#endif
	for (uint32_t i = 0; i < af.args; i++) {
		vector<int> clause(af.attackers[i].size()+1);
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			clause[j] = af.set_is_active_var[af.attackers[i][j]];
		}
		clause[af.attackers[i].size()] = af.arg_var[i];
		solver.add_clause(clause);
	}
}

}