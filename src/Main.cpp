/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "SetAF.h"
#include "CredAcceptance.h"
#include "SkeptAcceptance.h"
#include "SingleExtension.h"
#include "EnumerateExtensions.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <getopt.h>

using namespace std;

static int version_flag = 0;
static int usage_flag = 0;
static int formats_flag = 0;
static int problems_flag = 0;

task string_to_task(string problem)
{
	string tmp = problem.substr(0, problem.find("-"));
	if (tmp == "DC") return DC;
	if (tmp == "DS") return DS;
	if (tmp == "SE") return SE;
	if (tmp == "EE") return EE;
	return UNKNOWN_TASK;
}

semantics string_to_sem(string problem)
{
	problem.erase(0, problem.find("-") + 1);
	string tmp = problem.substr(0, problem.find("-"));
	if (tmp == "CO") return CO;
	if (tmp == "PR") return PR;
	if (tmp == "ST") return ST;
	if (tmp == "GR") return GR;
	if (tmp == "SST") return SST;
	if (tmp == "STG") return STG;
	if (tmp == "ID") return ID;
	return UNKNOWN_SEM;
}

void print_usage(string solver_name)
{
	cout << "Usage: " << solver_name << " -p <task> -f <file> [-a <query>]\n\n";
	cout << "  <task>      computational problem\n";
	cout << "  <file>      input SETAF in APX format\n";
	cout << "  <query>     query argument\n";
	cout << "Options:\n";
	cout << "  --help      Displays this help message.\n";
	cout << "  --version   Prints version and author information.\n";
	cout << "  --formats   Prints available file formats.\n";
	cout << "  --problems  Prints available computational tasks.\n";
}

void print_version(string solver_name)
{
	cout << solver_name << " version 2020.03.24\n" << "Andreas Niskanen, University of Helsinki <andreas.niskanen@helsinki.fi>\n";
}

void print_formats()
{
	cout << "[apx]\n";
}

void print_problems()
{
	vector<string> tasks = {"DC","DS","SE","EE"};
	vector<string> sems = {"CO","PR","ST","SST","STG","GR","ID"};
	cout << "[";
	for (uint32_t i = 0; i < tasks.size(); i++) {
		for (uint32_t j = 0; j < sems.size(); j++) {
			string problem = tasks[i] + "-" + sems[j];
			if (problem == "DS-GR" || problem == "EE-GR" || problem == "DS-ID" || problem == "EE-ID") {
				continue;
			}
			cout << problem;
			if (problem != "EE-STG") cout << ",";
		}
	}
	cout << "]\n";
}

int main(int argc, char ** argv)
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);

	if (argc == 1) {
		print_version(argv[0]);
		return 0;
	}

	const struct option longopts[] =
	{
		{"help", no_argument, &usage_flag, 1},
		{"version", no_argument, &version_flag, 1},
		{"formats", no_argument, &formats_flag, 1},
		{"problems", no_argument, &problems_flag, 1},
		{"p", required_argument, 0, 'p'},
		{"f", required_argument, 0, 'f'},
		{"a", required_argument, 0, 'a'},
		{0, 0, 0, 0}
	};

	int option_index = 0;
	int opt = 0;
	string task, file, query;

	while ((opt = getopt_long_only(argc, argv, "", longopts, &option_index)) != -1) {
		switch (opt) {
			case 0:
				break;
			case 'p':
				task = optarg;
				break;
			case 'f':
				file = optarg;
				break;
			case 'a':
				query = optarg;
				break;
			default:
				return 1;
		}
	}

	if (version_flag) {
		print_version(argv[0]);
		return 0;
	}

	if (usage_flag) {
		print_usage(argv[0]);
		return 0;
	}

	if (formats_flag) {
		print_formats();
		return 0;
	}

	if (problems_flag) {
		print_problems();
		return 0;
	}

	if (task.empty()) {
		cerr << argv[0] << ": Task must be specified via -p flag\n";
		return 1;
	}

	if (file.empty()) {
		cerr << argv[0] << ": Input file must be specified via -f flag\n";
		return 1;
	}

	ifstream input;
	input.open(file);

	if (!input.good()) {
		cerr << argv[0] << ": Cannot open input file\n";
		return 1;
	}

	SetAF af = SetAF();
	vector<pair<string,string>> atts;
	vector<pair<string,string>> mems;
	string line, arg, source, target;

	while (!input.eof()) {
		getline(input, line);
		line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());
		if (line.length() == 0 || line[0] == '/' || line[0] == '%') continue;
		if (line.length() < 6) cerr << "Warning: Cannot parse line: " << line << "\n";
		string op = line.substr(0,3);
		if (op == "arg") {
			if (line[3] == '(' && line.find(')') != string::npos) {
				arg = line.substr(4,line.find(')')-4);
				af.add_argument(arg);
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else if (op == "att") {
			if (line[3] == '(' && line.find(',') != string::npos && line.find(')') != string::npos) {
				source = line.substr(4,line.find(',')-4);
				target = line.substr(line.find(',')+1,line.find(')')-line.find(',')-1);
				atts.push_back(make_pair(source,target));
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else if (op == "mem") {
			if (line[3] == '(' && line.find(',') != string::npos && line.find(')') != string::npos) {
				source = line.substr(4,line.find(',')-4);
				target = line.substr(line.find(',')+1,line.find(')')-line.find(',')-1);
				mems.push_back(make_pair(source,target));
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else {
			cerr << "Warning: Cannot parse line: " << line << "\n";
		}
	}

	input.close();

	af.sem = string_to_sem(task);

	af.init_attackers();

	for (uint32_t i = 0; i < atts.size(); i++) {
		af.add_attack(atts[i]);
	}

	af.init_attacking_sets();

	for (uint32_t i = 0; i < mems.size(); i++) {
		af.add_argument_to_attack(mems[i]);
	}

	af.init_vars();

	switch (string_to_task(task)) {
		case DC:
		{
			if (query.empty()) {
				cerr << argv[0] << ": Query argument must be specified via -a flag\n";
				return 1;
			}
			bool cred_accepted = false;
			switch (string_to_sem(task)) {
				case CO:
					cred_accepted = CredAcceptance::complete(af, query);
					break;
				case PR:
					cred_accepted = CredAcceptance::preferred(af, query);
					break;
				case ST:
					cred_accepted = CredAcceptance::stable(af, query);
					break;
				case GR:
					cred_accepted = CredAcceptance::grounded(af, query);
					break;
				case SST:
					cred_accepted = CredAcceptance::semi_stable(af, query);
					break;
				case STG:
					cred_accepted = CredAcceptance::stage(af, query);
					break;
				case ID:
					cred_accepted = CredAcceptance::ideal(af, query);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return 1;
			}
			cout << (cred_accepted ? "YES" : "NO") << "\n";
			break;
		}
		case DS:
		{
			if (query.empty()) {
				cerr << argv[0] << ": Query argument must be specified via -a flag\n";
				return 1;
			}
			bool skept_accepted = false;
			switch (string_to_sem(task)) {
				case CO:
#if defined(DSCO_VIA_DCGR)
					skept_accepted = CredAcceptance::grounded(af, query);
#else
					skept_accepted = SkeptAcceptance::complete(af, query);
#endif
					break;
				case PR:
					skept_accepted = SkeptAcceptance::preferred(af, query);
					break;
				case ST:
					skept_accepted = SkeptAcceptance::stable(af, query);
					break;
				case SST:
					skept_accepted = SkeptAcceptance::semi_stable(af, query);
					break;
				case STG:
					skept_accepted = SkeptAcceptance::stage(af, query);
					break;
				case GR:
					cerr << argv[0] << ": Unsupported problem type. Suggestion: -p DC-GR\n";
					return 1;
				case ID:
					cerr << argv[0] << ": Unsupported problem type. Suggestion: -p DC-ID\n";
					return 1;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return 1;
			}
			cout << (skept_accepted ? "YES" : "NO") << "\n";
			break;
		}
		case SE:
		{
			switch (string_to_sem(task)) {
				case CO:
#if defined(SECO_VIA_SEGR)
					SingleExtension::grounded(af);
#else
					SingleExtension::complete(af);
#endif
					break;
				case PR:
					SingleExtension::preferred(af);
					break;
				case ST:
					SingleExtension::stable(af);
					break;
				case GR:
					SingleExtension::grounded(af);
					break;
				case SST:
					SingleExtension::semi_stable(af);
					break;
				case STG:
					SingleExtension::stage(af);
					break;
				case ID:
					SingleExtension::ideal(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return 1;
			}
			break;
		}
		case EE:
		{
			switch (string_to_sem(task)) {
				case CO:
					EnumerateExtensions::complete(af);
					break;
				case PR:
					EnumerateExtensions::preferred(af);
					break;
				case ST:
					EnumerateExtensions::stable(af);
					break;
				case SST:
					EnumerateExtensions::semi_stable(af);
					break;
				case STG:
					EnumerateExtensions::stage(af);
					break;
				case GR:
					cerr << argv[0] << ": Unsupported problem type. Suggestion: -p SE-GR\n";
					return 1;
				case ID:
					cerr << argv[0] << ": Unsupported problem type. Suggestion: -p SE-ID\n";
					return 1;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return 1;
			}
			break;
		}
		default:
			cerr << argv[0] << ": Unsupported problem\n";
			return 1;
	}

	return 0;
}