/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ARGU_FRAMEWORK_H
#define ARGU_FRAMEWORK_H

#include <vector>
#include <unordered_map>

enum task { DC, DS, SE, EE, UNKNOWN_TASK };
enum semantics { CO, PR, ST, GR, SST, STG, ID, UNKNOWN_SEM };

class SetAF {
public:

SetAF();

semantics sem;

uint32_t args;
uint32_t atts;
uint32_t count;

std::vector<std::string> int_to_arg;
std::unordered_map<std::string,uint32_t> arg_to_int;

std::vector<std::string> int_to_att;
std::unordered_map<std::string,uint32_t> att_to_int;

std::vector<std::vector<uint32_t>> attackers;
std::vector<std::vector<uint32_t>> attacking_sets;

std::vector<int> arg_var;
std::vector<int> range_var;
std::vector<int> set_is_active_var;
std::vector<int> attacked_by_active_set_var;

void add_argument(std::string arg);
void add_attack(std::pair<std::string,std::string> att);
void add_argument_to_attack(std::pair<std::string,std::string> mem);

void init_attackers();
void init_attacking_sets();
void init_vars();

};

#endif